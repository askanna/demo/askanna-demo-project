# AskAnna Data Science demo project

This is a template project containing the AskAnna Data Science demo project.
The demo project shows a potential data science project with jobs to train
models and serve results in AskAnna. The demo is based on the data science
template.

Using the AskAnna CLI you can run the next command to create a new project
in AskAnna with this template. If creating the project is finished, you will
find a new local directory with this demo project.

```bash
askanna create "AskAnna Demo Project" --template https://gitlab.com/askanna/demo/askanna-demo-project.git --push
```
