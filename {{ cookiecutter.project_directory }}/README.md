# AskAnna Data Science demo project

This is a simple project to demonstrate what AskAnna can do, and how you can setup AskAnna to run
your code. We defined three jobs and the purpose of each jobs is a little bit different. In the
`askanna.yml` you can find what every job runs and which files are saved after a run.

## Project Organization

    ├── askanna.yml        <- Configuration file for AskAnna
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    ├── data
    │   ├── input          <- The original, immutable data (dump) 
    │   ├── interim        <- Intermediate data sets
    │   └── processed      <- The final prepped data sets for modeling
    │
    ├── docs               <- A central location for your project documentation
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- A location for your project related Jupyter notebooks
    │
    ├── src                <- Source code for use in this project
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │
    │   └── models         <- Scripts to train models and use trained models to serve a result
    │
    ├── .env               <- Environment file for local development
    └── README.md          <- This document
